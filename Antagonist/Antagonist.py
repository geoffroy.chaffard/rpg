import abc
from Antagonist.AntagonistData.Class import Classe
from Antagonist.Character.Skills import Skill
from Antagonist.Character.Actions import Action
from Antagonist.AntagonistData.Stats.Active.ComplexStat import Initiative
from Antagonist.AntagonistData.Stats.Active.ComplexStat import Health

class Antagonist(abc.ABC):

     @abc.abstractmethod
     def _Antagonist__set_classe(self, classe):
          raise NotImplementedError

     @abc.abstractmethod
     def get_classe(self) -> Classe:
          raise NotImplementedError

     @abc.abstractmethod
     def _Antagonist__set_skills(self, skills):
          raise NotImplementedError

     @abc.abstractmethod
     def get_skills(self) -> Skill:
          raise NotImplementedError
     
     @abc.abstractmethod
     def _Antagonist__set_name(self, name):
          raise NotImplementedError

     @abc.abstractmethod
     def get_name(self) -> str:
          raise NotImplementedError

     @abc.abstractmethod
     def _Antagonist__set_initiative(self, initiative):
          raise NotImplementedError

     @abc.abstractmethod
     def get_initiative(self) -> Initiative:
          raise NotImplementedError

     @abc.abstractmethod
     def _Antagonist__set_health(self, health):
          raise NotImplementedError

     @abc.abstractmethod
     def get_health(self) -> Health:
          raise NotImplementedError

     @abc.abstractmethod
     def _Antagonist__set_actions(self, actions):
          raise NotImplementedError
     
     @abc.abstractmethod
     def get_actions(self) -> dict:
          raise NotImplementedError

     @abc.abstractmethod
     def choose_action(self) -> Action:
          raise NotImplementedError

     @abc.abstractmethod
     def take_damage(self, damage):
          raise NotImplementedError