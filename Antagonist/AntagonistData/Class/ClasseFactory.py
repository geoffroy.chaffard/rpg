from Util.YAMLLoader import YAMLLoader as yml_loader
from Antagonist.AntagonistData.Class.Classe import Classe

class ClasseFactory():

     def __init__(self):
          self.__loader = yml_loader('Config/include.yml')
          self.__set_classe_config()

     def get_loader(self) -> yml_loader:
          return self.__loader

     def __set_classe_config(self):
          self.__classe_config = self.get_loader().load_yaml_from_include('classe')

     def get_classe_config(self) -> dict:
          return self.__classe_config

     def get_classe_from_conf(self, classe):
          return self.get_classe_config()[classe]
     
     def get_classe(self, className) -> Classe:
          return Classe(self.get_classe_from_conf(className)['name'], self.get_classe_from_conf(className)['description'])