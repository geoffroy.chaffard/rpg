class Classe():
     '''
          TODO :
                    Rajouter les Skills dans la classe
     '''
     def __init__(self, name, description):
          self.__set_description(description)
          self.set_name(name)
          
     def set_name(self, name):
          self.__name = name

     def get_name(self) -> str:
          return self.__name

     def __set_description(self, description):
          self.__description = description

     def get_description(self) -> str:
          return self.__description

     def __str__(self):
          return '{}\n\nDescription : {}\n\t'.format(str(self.get_name()), str(self.get_description()))
