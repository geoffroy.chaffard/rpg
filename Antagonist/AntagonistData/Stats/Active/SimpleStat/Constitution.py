from .SimpleStatistic import SimpleStatistic

class Constitution(SimpleStatistic):

     def __init__(self, value):
          name = 'constitution'
          description = 'The ability of the character to take damage'
          super().__init__(name, description, value)
