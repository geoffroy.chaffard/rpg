from .SimpleStatistic import SimpleStatistic

class Agility(SimpleStatistic):

     def __init__(self, value):
          name = 'agility'
          description = 'The ability of the character to move his body'
          super().__init__(name, description, value)
          