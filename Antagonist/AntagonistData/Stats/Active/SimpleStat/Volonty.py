from .SimpleStatistic import SimpleStatistic

class Volonty(SimpleStatistic):

     def __init__(self, value):
          name = 'volonty'
          description = 'The mental strenght of the character'
          super().__init__(name, description, value)

