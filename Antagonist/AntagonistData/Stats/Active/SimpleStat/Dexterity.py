from .SimpleStatistic import SimpleStatistic

class Dexterity(SimpleStatistic):

     def __init__(self, value):
          name = 'dexterity'
          description = 'The ability of the character to use object'
          super().__init__(name, description, value)
