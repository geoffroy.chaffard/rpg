from .SimpleStatistic import SimpleStatistic

class Intelligence(SimpleStatistic):

     def __init__(self, value):
          name = 'intelligence'
          description = 'Reflect the intelligence of the character'
          super().__init__(name, description, value)
          