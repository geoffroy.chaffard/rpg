from .SimpleStatistic import SimpleStatistic

class Strenght(SimpleStatistic):

     def __init__(self, value):
          name = 'strenght'
          description = 'The physic power of the character'
          super().__init__(name, description, value)
