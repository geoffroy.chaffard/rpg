from .SimpleStatistic import SimpleStatistic

class Resistance(SimpleStatistic):

     def __init__(self, value):
          name = 'resistance'
          description = 'The ability of the character body to absord and cure from poison'
          super().__init__(name, description, value)
