from .ComplexeStatistic import ComplexeStatistic

class Initiative(ComplexeStatistic):

     def __init__(self, intelligence, strenght, dexterity, agility, volonty):
          initiative = self.__calcul_initiative(intelligence, strenght, dexterity, agility, volonty)
          name = 'initiative'
          description = 'The initiative determine how fast a player will play his turn'
          super().__init__(name, description, initiative)

     def __calcul_initiative(self, intelligence, strenght, dexterity, agility, volonty):
          return intelligence + strenght + dexterity + agility + volonty