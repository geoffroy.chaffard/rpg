from .ComplexeStatistic import ComplexeStatistic

class Attack(ComplexeStatistic):

     def __init__(self, stat1, stat2):
          attack = self.__calcul_attack(stat1, stat2)
          name = 'attack'
          description = 'The amout of damage deal with an attack'
          super().__init__(name, description, attack)

     def __calcul_attack(self, stat1, stat2):
          return stat1 * 2 + stat2