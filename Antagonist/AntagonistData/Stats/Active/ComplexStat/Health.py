from .ComplexeStatistic import ComplexeStatistic

class Health(ComplexeStatistic):

     def __init__(self, const, res, vol):
          Health = self.__calcul_health(const, res, vol)
          name = 'health'
          description = 'The life of the character'
          super().__init__(name, description, Health)

     def __calcul_health(self, const, res, vol):
          return const + res + vol
