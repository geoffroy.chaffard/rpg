from .ComplexeStatistic import ComplexeStatistic

class Precision(ComplexeStatistic):

     def __init__(self, dext, const, intel, strenght):
          precision = self.__calcul_precision(dext, const, intel, strenght)
          name = 'precision'
          description = 'The odd of the characher to touch his target'
          super().__init__(name, description, precision)

     def __calcul_precision(self, dext, const, intel, strenght):
          return 50 + (dext * 2 - max(const, intel, strenght))
