from .ComplexeStatistic import ComplexeStatistic

class Defense(ComplexeStatistic):

     def __init__(self, stat1, stat2):
          defense = self.__calcul_defense(stat1, stat2)
          name = 'defense'
          description = 'The amount of damage reduce from an attack'
          super().__init__(name, description, defense)

     def __calcul_defense(self, stat1, stat2):
          return stat1 * 2 + stat2 