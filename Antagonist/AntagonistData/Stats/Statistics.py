from Antagonist.AntagonistData.Stats import Statistic
from Antagonist.AntagonistData.Stats.Active.ComplexStat import Health, Precision, Initiative, Attack
from Antagonist.AntagonistData.Stats.Active.SimpleStat import Intelligence, Strenght, Agility, Dexterity, Resistance, Constitution, Volonty

class Statistics():

     def __init__(self, intelligence, strenght, dexterity, agility, constitution, resistance, volonty):
          self.__statistics = {}
          self.set_intel(Intelligence.Intelligence(intelligence))
          self.set_strenght(Strenght.Strenght(strenght))
          self.set_agility(Agility.Agility(agility))
          self.set_dext(Dexterity.Dexterity(dexterity))
          self.set_res(Resistance.Resistance(resistance))
          self.set_const(Constitution.Constitution(constitution))
          self.set_vol(Volonty.Volonty(volonty))
          self.set_health(Health.Health(constitution, resistance, volonty))
          self.set_precision(Precision.Precision(dexterity, constitution, intelligence, strenght))
          self.set_initiative(Initiative.Initiative(intelligence, strenght, dexterity, agility, volonty))
          self.set_attack(Attack.Attack(strenght, intelligence))

     '''
          Getters
     '''

     def get_intel(self) -> Intelligence:
          return self.__statistics['intellligence']
     
     def get_strenght(self) -> Strenght:
          return self.__statistics['strenght']

     def get_dext(self) -> Dexterity:
          return self.__statistics['dexterity']

     def get_agility(self) -> Agility:
          return self.__statistics['agility']

     def get_const(self) -> Constitution:
          return self.__statistics['constitution']

     def get_res(self) -> Resistance:
          return self.__statistics['resistance']

     def get_vol(self) -> Volonty:
          return self.__statistics['volonty']

     def get_health(self) -> Health:
          return self.__statistics['health']

     def get_precision(self) -> Precision:
          return self.__statistics['precision']

     def get_initiative(self) -> Initiative:
          return self.__statistics['initiative']

     def get_attack(self) -> Attack:
          return self.__statistics['attack']

     def has_attack(self, attack):
          return attack in self.__statistics

     '''
          Setters
     '''

     def set_intel(self, intel):
          self.__statistics['intelligence'] = intel
     
     def set_strenght(self, strenght):
          self.__statistics['strenght'] = strenght 

     def set_dext(self, dext):
          self.__statistics['dexterity'] = dext

     def set_agility(self, agi):
          self.__statistics['agility'] = agi

     def set_const(self, const):
          self.__statistics['constitution'] = const

     def set_res(self, res):
          self.__statistics['resistance'] = res

     def set_vol(self, vol):
          self.__statistics['volonty'] = vol

     def set_health(self, health):
          self.__statistics['health'] = health

     def set_precision(self, precision):
          self.__statistics['precison'] = precision

     def set_initiative(self, initiative):
          self.__statistics['initiative'] = initiative
     
     def set_attack(self, attack):
          self.__statistics['attack'] = attack
     
     def __str__(self):
          strg = ''
          for statistic in self.__statistics.values():
               strg+= str(statistic)
          return strg

     def change_life(self, value):
          new_health = self.get_health().get_value() + value
          self.get_health().set_value(new_health)