class Statistic():

     def __init__(self, name, description, value):
          self.__name = name
          self.__description = description
          self.__value = value

     def get_value(self):
          return self.__value

     def set_value(self, value):
          self.__value = value

     def get_description(self):
          return self.__description

     def set_description(self, description):
          self.__description = description

     def get_name(self):
          return self.__name

     def set_name(self, name):
          self.__name = name

     def __str__(self):
          return 'Nom : {}\n\tValeur : {}\n'.format(self.get_name(), self.get_value())
