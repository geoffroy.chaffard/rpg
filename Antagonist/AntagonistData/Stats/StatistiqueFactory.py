from Util.YAMLLoader import YAMLLoader as yml_loader
from Antagonist.AntagonistData.Stats.Statistics import Statistics

class StatisticFactory():

     def __init__(self):
          self.__loader = yml_loader('Config/include.yml')
          self.__set_statistic_config()

     def get_loader(self) -> yml_loader:
          return self.__loader

     def __set_statistic_config(self):
          self.__statistic_config = self.get_loader().load_yaml_from_include('character')

     def get_statistic_config(self) -> dict:
          return self.__statistic_config

     def get_statistic_from_conf(self, classe):
          return self.get_statistic_config()[classe]

     def get_statistics(self, statistics):
          return Statistics(statistics['intelligence'], statistics['strenght'], statistics['dexterity'], statistics['agility'], \
                            statistics['constitution'], statistics['resistance'], statistics['volonty'])
     
     def get_statistic(self, className):
          return self.get_statistics(self.get_statistic_from_conf(className))