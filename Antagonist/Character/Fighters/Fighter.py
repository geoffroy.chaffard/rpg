from Antagonist.AntagonistData.Class.Classe import Classe
from Antagonist.Antagonist import Antagonist
from Antagonist.AntagonistData.Stats.Active.ComplexStat import Health
from Antagonist.AntagonistData.Stats.Active.ComplexStat import Initiative
from Antagonist.Character.Actions import Action

#Util
from Util.cmp import cmp

class Fighter():

     #Actions = Skills
     def __init__(self, antagonist: Antagonist):
          self.__set_antagonist(antagonist)
          self.__set_actions(self.get_antagonist().get_actions())

     '''
          Getters and Setters
     '''

     def __set_antagonist(self, antagonist):
          self.__antagonist = antagonist

     def get_antagonist(self) -> Antagonist :
          return self.__antagonist

     def get_antagonist_name(self) -> str:
          return self.get_antagonist().get_name()

     def get_antagonist_initiative(self) -> Initiative:
          return self.get_antagonist().get_initiative().get_value()

     def get_antagonist_health(self) -> Health:
          return self.get_antagonist().get_health().get_value()

     def __set_actions(self, actions):
          self.__actions = actions

     def get_actions(self) -> Action:
          return self.__actions

     def get_damage(self, target: Antagonist):
          raise NotImplementedError

     def take_damage(self, damage):
          raise NotImplementedError

     '''
          Util
     '''
     def __lt__(self, other):
          return self.__cmp__(other)

     def __gt__(self, other):
        return self.__cmp__(other)

     def __eq__(self, other):
        return self.__cmp__(other)

     def __cmp__(self, other):
          return cmp(-1*self.get_antagonist_initiative(), -1*other.get_antagonist_initiative())
     

     '''
          Function
     '''

     def choose_action(self):
          input('Choose an action among thoose below :\n\t{}'.format(' '.join(self.get_antagonist().get_action().get_name())))
          self.get_actions().do_action()

     '''
          A définir, le choix de cible parmi plusieurs.
     '''
     def choose_target(self, target, nb_target = 1):
          '''
               target is a list.
          '''
          if nb_target is 1:
               '''
                    If we have only one target and only one ennemy, we return this ennemy.
                    Else we choose an ennemy.
               '''
               if len(target) is 1:
                    return target[0]
               else:
                    pass
               '''
                    If we have more than one target and only one ennemy, we return this ennemy.
                    Else we choose has many ennemy as there are targets.
               '''
          elif nb_target > 1:
               if len(target) is 1:
                    return target[0]
               else:
                    pass
               '''
                    If the nb of target is -2, the target are the allies.
               '''
          elif nb_target == -2:
               pass
               '''
                    If the nb of target is -3, the target are the ennemies.
               '''
          elif nb_target == -3:
               pass
               '''
                    If the nb of target is -1, the target is the antagonist himself.
               '''
          elif nb_target == -1:
               return self.get_antagonist()
          else:
               pass