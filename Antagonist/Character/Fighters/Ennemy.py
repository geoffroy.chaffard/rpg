from .Fighters import Fighters
from Antagonist.Antagonist import Antagonist

class Ennemy(Fighters):

     def __init__(self):
          super().__init__()

     def get_damage(self, target: Antagonist):
          raise NotImplementedError

     def take_damage(self, damage):
          raise NotImplementedError
