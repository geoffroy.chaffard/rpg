import queue as Q
from .Fighter import Fighter

class Fighters():

     def __init__(self):
          self.__fighters = Q.PriorityQueue()

     def get_fighters(self) -> Q.PriorityQueue:
          return self.__fighters

     def add_fighter(self, fighter):
          self.get_fighters().put(fighter)

     def get_next_fighter(self) -> Fighter:
          return self.get_fighters().get()
     
     def has_next_fighter(self):
          return self.get_fighters().empty()

     def remove_all_fighters(self):
          while self.has_next_fighter():
               self.get_next_fighter()

     def __len__(self):
          return self.get_fighters().qsize()