from Antagonist.Character.Skills.Skill import Skill
from Antagonist.Character.Skills.Skills import Skills
from Util.YAMLLoader import YAMLLoader as yml_loader

class SkillFactory():

     def __init__(self):
          self.__loader = yml_loader('Config/include.yml')
          self.__set_skills_config()

     def get_loader(self) -> yml_loader:
          return self.__loader

     def __set_skills_config(self):
          self.__skills_config = self.get_loader().load_yaml_from_include('skills')

     def get_skills_config(self) -> dict:
          return self.__skills_config
     
     def get_skills_from_class(self, className):
          skills = Skills()
          for skill in self.get_skills_config()[className].values():
               skills.add_skill(Skill(skill['name'], skill['description'], skill['damage'], skill['target'], skill['effect'], skill['range']))
          return skills

     def get_skills(self, className):
          return self.get_skills_from_class(className)

