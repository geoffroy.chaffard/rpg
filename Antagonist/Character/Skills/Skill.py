from Antagonist.Character.Actions import Action

class Skill():

     def __init__(self, name, description, damage, nb_target, other_effects, aiming_range = 'Je sais pas encore si j\'en mets'):
          self._Action__set_name(name)
          self._Action__set_description(description)
          self.__set_damage(damage)
          self.__set_nb_target(nb_target)
          self.__set_other_effects(other_effects)
          self.__set_aiming_range(aiming_range)

     def _Action__set_name(self, name):
          self.__name = name

     def get_name(self) -> str:
          return self.__name

     def _Action__set_description(self, description):
          self.__description = description

     def get_description(self) -> str:
          return self.__description

     def __set_damage(self, damage):
          self.__damage = damage

     def get_damage(self) -> int:
          return self.__damage

     def __set_nb_target(self, nb_target):
          self.__nb_target = nb_target

     def get_nb_target(self) -> int:
          return self.__nb_target

     def __set_other_effects(self, other_effetcs):
          self.__other_effetcs = other_effetcs

     def get_other_effetcs(self):
          return self.__other_effetcs

     def __set_aiming_range(self, aiming_range):
          self.__aiming_range = aiming_range

     def get_aiming_range(self):
          return self.__aiming_range

     def has_target(self):
          return True

     def do_action(self, target = None):
          '''
               We get the numbers of targets. 
               If the number of target in target is different from the number of target in self.__nb_target, we raise an exception
               If the number of target is equal to 0, we apply the effect of the action to the target.
               if we have one target, we apply the damage and the effect.
               if we have more than one target, we loop over target and apply damage and effect to each target.
               Then we return the target.
          '''
          if not target:
               return None
          nb_target = len(target)
          if not nb_target is self.get_nb_target():
               pass
          if nb_target == 0:
               pass
          elif nb_target == 1:
               target.take_damage(self.get_damage())
          elif nb_target > 1:
               for index in range(len(target)):
                    target_tmp = target[index]
                    target_tmp.take_damage(self.get_damage)
                    target[index] = target_tmp
          return target

     def __str__(self):
          return ' {} : {} | Damage {} | Nombre de cible : {} | Effet Complemantaire : {}'.format(self.get_name(), self.get_description(), self.get_damage(), self.get_nb_target(), self.get_other_effetcs())

