from Antagonist.Character.Skills import Skill

class Skills():
     
     def __init__(self):
          self.__skills =  []

     def __set_skills(self, skills):
          self.__skills = skills

     def add_skill(self, skill):
          if skill not in self.__skills:
               self.__skills.append(skill)

     def add_all_skills(self, skills):
          for skill in skills:
               self.add_skill(skill)

     def del_skill(self, skill):
          if skill in self.get_all_skills():
               del self.__skills[(self.get_all_skills().index(skill))]
     
     def del_all_skills(self):
          self.__skills = []

     def get_skill(self, skill) -> Skill:
          if skill in self.get_all_skills():
               return self.__skills[self.__skills.index(skill)]
          else:
               return False

     def get_all_skills(self):
          return self.__skills

     def __str__(self) -> str:
          strg = ''
          for skill in self.get_all_skills():
               strg += str(skill) + '\n\t\t'
          return strg
