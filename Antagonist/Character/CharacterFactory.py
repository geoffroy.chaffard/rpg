from Antagonist.AntagonistData.Class.ClasseFactory import ClasseFactory
from Antagonist.AntagonistData.Stats.StatistiqueFactory import StatisticFactory
from Antagonist.Character.Skills.SkillFactory import SkillFactory
from Antagonist.Character.Character import Character

class CharacterFactory():

     def __init__(self):
          self.__classe_factory = ClasseFactory()
          self.__statistic_factory = StatisticFactory()
          self.__skill_factory = SkillFactory()

     def get_classe_factory(self) -> ClasseFactory:
          return self.__classe_factory

     def get_statistic_factory(self) -> StatisticFactory:
          return self.__statistic_factory

     def get_skill_factory(self) -> SkillFactory:
          return self.__skill_factory
     
     def get_character(self, className):
          statistics = self.get_statistic_factory().get_statistic(className)
          classe = self.get_classe_factory().get_classe(className)
          skills = self.get_skill_factory().get_skills(className)
          return Character(classe, statistics, skills)