from Antagonist.AntagonistData.Stats.Statistics import Statistics
from Antagonist.Antagonist import Antagonist
from Antagonist.Character.Fighters.Fighter import Fighter
from Antagonist.AntagonistData.Class.Classe import Classe
from Antagonist.Character.Skills.Skills import Skills
from Antagonist.Character.Actions.Action import Action
from Antagonist.AntagonistData.Stats.Active.ComplexStat.Initiative import Initiative
from Antagonist.AntagonistData.Stats.Active.ComplexStat.Health import Health
from Antagonist.Character.Actions.BattleActions.AttackBattleAction import AttackBattleAction
from Antagonist.Character.Actions.BattleActions.SkillsBattleAction import SkillsBattleAction
from Antagonist.Character.Actions.BattleActions.NoActionBattleAction import NoActionBattleAction
from Util.cmp import cmp

class Character(Antagonist, Fighter):

     def __init__(self, classe, statistics, skills):
          self.__set_statistics(statistics)
          self._Antagonist__set_classe(classe)
          self._Antagonist__set_skills(skills)
          self._Antagonist__set_actions()
          super().__init__(self)
        
     def __set_statistics(self, statistics):
          self.__statistics = statistics

     def get_statistics(self) -> Statistics:
          return self.__statistics

     def _Antagonist__set_classe(self, classe):
          self.__classe = classe

     def get_classe(self) -> Classe:
          return self.__classe
     
     def get_skills(self) -> Skills:
          return self.__skills

     def _Antagonist__set_skills(self, skills):
          self.__skills = skills
     
     def _Antagonist__set_name(self, name):
          self.get_classe().set_name(name)

     def get_name(self) -> str:
          return self.get_classe().get_name()

     def _Antagonist__set_initiative(self, initiative):
          self.get_statistics().set_initiative(initiative)

     def get_initiative(self) -> Initiative:
          return self.get_statistics().get_initiative()

     def _Antagonist__set_health(self, health):
          self.get_statistics().set_health(health)

     def get_health(self) -> Health:
          return self.get_statistics().get_health()

     def _Antagonist__set_actions(self):
          self.__actions = {}
          self.__actions['skills'] = SkillsBattleAction('skills', 'Utiliser une compétence', self.get_skills())
          self.__actions['attack'] = AttackBattleAction('attack', 'Attaque l\'adversaire', self.get_statistics().get_attack())
          self.__actions['noaction'] = NoActionBattleAction('noaction', 'Le personnage n\'effectu action')

     def get_actions(self) -> dict:
          return self.__actions

     def has_action(self, action):
          return action in self.get_actions()

     def get_action(self, action) -> Action:
          return self.get_actions()[action]

     def choose_action(self) -> Action:
          print_actions = ' - '.join(action for action in self.get_actions()) + '\n'
          action_select = input('Choississez l\'action à effectuer pendant votre tour :\n\t{}'.format(print_actions))
          while (not self.has_action(action_select)) and (not action_select == 'exit') and (not action_select == 'quit'):
               del action_select
               print('Vous avez choisi une option qui n\'était pas valide, veuillez choisir à nouveau\n')
               action_select = input('Choississez l\'action à effectuer pendant votre tour :\n\t{}'.format(print_actions))
          return self.__actions[action_select]

     def do_action(self, target):
          action = self.choose_action()
          return action.do_action(target)
          
     def take_damage(self, damage):
          if not isinstance(damage, int):
               damage = damage.get_value()
          self.get_statistics().change_life(-1*damage)
          print('Le {} à pris {} de dégat, il a maintenant {} de vie\n'.format(self.get_classe().get_name(), damage, self.get_health().get_value()))

     def __str__(self):
          return 'Classe : ' + str(self.get_classe()) + '\n\t\t---------------------------------\n\n' + 'Statistique : \n' + str(self.get_statistics()) + '\n\t\t---------------------------------\n\n' + str(self.get_skills()) 
     
     def __len__(self):
          return 1

     def __cmp__(self, other):
          return cmp(-1*self.get_antagonist_initiative(), -1*other.get_antagonist_initiative())

     def print_health(self):
          print(' Le personnage {} à {}hp\n'.format(self.get_name(), self.get_statistics().get_health().get_value()))