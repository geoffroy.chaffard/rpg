import abc

class Action(abc.ABC):

     @abc.abstractmethod
     def _Action__set_name(self, name):
          raise NotImplementedError

     @abc.abstractmethod
     def get_name(self) -> str:
          raise NotImplementedError

     @abc.abstractmethod
     def _Action__set_description(self, skill):
          raise NotImplementedError

     @abc.abstractmethod
     def get_description(self) -> str:
          raise NotImplementedError

     @abc.abstractmethod
     def has_target(self):
          raise NotImplementedError

     @abc.abstractmethod
     def has_multiple_choice(self):
          raise NotImplementedError

     '''
          Take a process from a skill and apply it to the ennemy
     '''

     @abc.abstractmethod
     def do_action(self, target):
          raise NotImplementedError