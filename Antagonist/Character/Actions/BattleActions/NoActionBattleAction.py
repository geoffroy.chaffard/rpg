from .SimpleBattleAction import SimpleBattleAction
from Antagonist.Antagonist import Antagonist

class NoActionBattleAction(SimpleBattleAction):
     
     def __init__(self, name, description):
          super().__init__(name, description)

     def has_damage(self):
          return False

     def has_multiple_choice(self):
          return False

     def do_action(self, target: Antagonist) -> Antagonist:
          return target