from .SimpleBattleAction import SimpleBattleAction
from Antagonist.Antagonist import Antagonist

class AttackBattleAction(SimpleBattleAction):
     
     def __init__(self, name, description, damage):
          super().__init__(name, description)
          self.__set_damage(damage)

     def has_damage(self):
          return True

     def has_multiple_choice(self):
          return False

     def __set_damage(self, damage):
          self.__damage = damage

     def get_damage(self) -> int:
          return self.__damage

     def do_action(self, target: Antagonist) -> Antagonist:
          print(' attaque le {}\n\t'.format(target.get_name()))
          return target.take_damage(self.get_damage())
