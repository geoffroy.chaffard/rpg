from Antagonist.Character.Actions.Action import Action
from Util.name import Name

class BattleAction(Action):
     
     def __init__(self, name, description):
          self._Action__set_name(name)
          self._Action__set_description(description)

     def _Action__set_name(self, name):
          self.__name = Name(name)

     def get_name(self) -> str:
          return self.__name

     def _Action__set_description(self, description):
          self.__description = description

     def get_description(self) -> str:
          return self.__description

     def has_target(self):
          return True

     def has_damage(self):
          raise NotImplementedError

     def has_multiple_choice(self):
          raise NotImplementedError

     def do_action(self, target):
          raise NotImplementedError
