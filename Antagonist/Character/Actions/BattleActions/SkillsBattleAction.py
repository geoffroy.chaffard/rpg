from Antagonist.Character.Actions.BattleActions.SimpleBattleAction import SimpleBattleAction
from Antagonist.Antagonist import Antagonist
from Antagonist.Character.Skills.Skills import Skills
from Antagonist.Character.Skills.Skill import Skill

class SkillsBattleAction(SimpleBattleAction):
     
     def __init__(self, name, description, skills):
          self.__set_skills(skills)
          super().__init__(name, description)

     def has_damage(self):
          return True

     def has_multiple_choice(self):
          return True

     def get_damage(self, skill) -> int:
          return self.get_skills().get_skill(skill).get_damage()

     def __set_skills(self, skills: Skills):
          self.__skills = skills

     def get_skills(self) -> Skills:
          return self.__skills

     def is_skill_in_skills(self, skill_select):
          for skill in self.get_skills().get_all_skills():
               if skill.get_name() == skill_select:
                    return True
          return False
     
     def get_skill_by_name(self, name):
          for skill in self.get_skills().get_all_skills():
               if skill.get_name() == name:
                    return skill
          return False

     def choose_action(self) -> Skill:
          skill_list = self.get_skills().get_all_skills()
          print_skills = ' - '.join(skill.get_name() for skill in skill_list )+'\n'
          skill_select = input('Choississez l\'action à effectuer pendant votre tour :\n\t{}'.format(print_skills))
          is_skill_in_skills = self.is_skill_in_skills(skill_select)
          while not is_skill_in_skills and not skill_select == 'exit' and not skill_select == 'quit':
               print('Vous avez choisi une option qui n\'était pas valide, veuillez choisir à nouveau\n')
               skill_select = input('Choississez l\'action à effectuer pendant votre tour :\n\t{}'.format(print_skills))
          return self.get_skill_by_name(skill_select)

     def do_action(self, target: Antagonist) -> Antagonist:
          skill_choose = self.choose_action()
          return target.take_damage(skill_choose.get_damage())