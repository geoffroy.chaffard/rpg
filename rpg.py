from Antagonist.Character.Character import Character
from Antagonist.Character.CharacterFactory import CharacterFactory
from Fight.Fight import Fight

'''
    Accueil le lancement du script
'''
def main():
    
    char_factory = CharacterFactory()
    char1 = char_factory.get_character('warrior')
    char2 = char_factory.get_character('bowman')


    char1.print_health()
    char2.print_health()

    fight = Fight(char1, char2)

    fight.start()
    char1.print_health()
    char2.print_health() 

if __name__ == "__main__":
    main()