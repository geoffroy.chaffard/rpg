class Name():

     def __init__(self, name):
          self.set_name(name)

     def set_name(self, name):
          self.__name = name

     def get_name(self) -> str:
          return self.__name

     def __str__(self):
          return 'Nom : {} '.format(self.get_name())