import importlib

class Invoker():

     __config = None
     __columns = dict()
     __command = dict()

     def __init__(self, config, command):
          self.__set_config(config)
          self.__set_command(command)
     
     def __set_config(self, config):
          self.__config = config

     def get_config(self):
          return self.__config

     def __set_command(self, command):
          self.__command = command

     def get_command(self):
          return self.__command

     def get_action_from_config(self, action):
          pass

     def get_param_from_config(self, action, method):
          pass

     def execute_command(self, somethings):
          action_list = dict()
          for action, method in self.get_command():
               obj_tmp = Invoker.get_class(self.get_action_from_config(action))
               param_tmp = self.get_param_from_config(self.get_action_from_config(action), action)
               action_list[action] = obj_tmp.excecute(somethings, param_tmp)
          return action_list

     @staticmethod
     def get_class(cls):
          klass = globals()[cls]
          return klass()