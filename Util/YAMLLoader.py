import yaml

class NoPathForYamlError(Exception):
     pass

class NoPathInIncludesError(Exception):
     pass

class YAMLLoader():

     def __init__(self, path_to_conf = None):
          self.__set_config(path_to_conf)

     def __set_config(self, path_to_conf):
          self.__config = self.load_yaml(path_to_conf)
     
     def load_yaml(self, path):
          if path:
               with open(path, 'r') as stream:
                    try:
                         return yaml.safe_load(stream)
                    except yaml.YAMLError as exc:
                         print('YAMLLoader exception : ' + str(exc))
          else:
               raise NoPathForYamlError()

     def get_config(self):
          return self.__config

     def load_yaml_from_include(self, include):
          if include in self.get_config()['includes']:
               return self.load_yaml(self.get_config()['includes'][include])
          else:
               raise NoPathInIncludesError()