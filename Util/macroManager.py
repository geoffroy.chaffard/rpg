#Global
from __future__ import unicode_literals

#Util


class NoActionException(Exception):
     pass

class NoOperationActionException(Exception):
     pass

class NoOperationMacroException(Exception):
     pass

class macroInterpreter():

     def __init__(self):
          pass

     def start(self, macro_config, SOMETHINGS):
          return self.__execute(macro_config, SOMETHINGS)

     def __execute(self, macro_config, SOMETHINGS):
          try:
               if not 'macro' in macro_config:
                    if 'actions' in macro_config:
                         if len(macro_config['actions']) > 1:
                              if not 'operation_action' in macro_config:
                                   raise NoOperationActionException()
                              else:
                                   return self.__get_function_result_with_actions(macro_config, SOMETHINGS)
                         else:
                              return self.__get_function_result_with_actions(macro_config, SOMETHINGS)
                    else:
                         raise NoActionException()
               else:
                    if 'actions' in macro_config and not 'operation_macro' in macro_config:
                         raise NoOperationMacroException()
                    result = eval('{result1}{operande}{result2}'.format(result1 = self.__get_function_result_with_actions(macro_config['macro'], SOMETHINGS)\
                                                                      , operande = macro_config['operation_macro']\
                                                                      , result2 = self.__execute(macro_config['macro'], SOMETHINGS)))
                    return result
               if not 'operation_action' in macro_config:
                    raise NoOperationActionException()
          except NoOperationActionException:
               pass
          except NoActionException:
               pass
          except NoOperationMacroException:
               pass

     def __get_function_result_with_actions(self, macro, SOMETHINGS):
          result_list = []
          if 'parameters' in macro :
               for index_action in range(len(macro['action'])) :
                    function = macro['parameters']['action_params'][index_action]['function_name']
                    value = macro['parameters'][index_action]['value']
                    result_list.append(str(self.__evaluate_action(function, value, SOMETHINGS)))
               if len(result_list) > 0 :
                    return eval(' {} '.format(macro['operation_action']).join(result_list))
               else:
                    return False
          else:
               return False
     
     def __evaluate_action(self, function, value, SOMETHINGS):
          if function and value:
               method_name = str(function)
               default = 'invalid_method'
               func = getattr(self, method_name, lambda x: default)
               return func(SOMETHINGS, value)
          return False