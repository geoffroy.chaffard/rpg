from Antagonist.Character.Fighters.Fighters import Fighters
from Antagonist.Character.Fighters.Fighter import Fighter
from Antagonist.Character.Actions.Action import Action

class Round():

     def __init__(self, allies: list, ennemies: list):
          self.__set_fighters(allies, ennemies)
          self.__set_allies(allies)
          self.__set_ennemies(ennemies)

     '''
          Getters and Setters
     '''

     def __set_fighters(self, allies, ennemies):
          self.__fighters = Fighters()
          for ally in allies:
               self.__fighters.add_fighter(ally)
          for ennemy in ennemies:
               self.__fighters.add_fighter(ennemy)

     def get_fighters(self) -> Fighters:
          return self.__fighters

     def __set_allies(self, allies):
          self.__allies = Fighters()
          self.__list_allies = []
          for ally in allies:
               self.__allies.add_fighter(ally)
               self.__list_allies.append(ally)

     def get_allies(self) -> Fighters:
          return self.__allies

     def get_list_allies(self):
          return self.__list_allies

     def __set_ennemies(self, ennemies):
          self.__ennemies = Fighters()
          self.__list_ennemies = []
          for ennemy in ennemies:
               self.__ennemies.add_fighter(ennemy)
               self.__list_ennemies.append(ennemy)

     def get_ennemies(self) -> Fighters:
          return self.__ennemies

     def get_list_ennemies(self) -> list:
          return self.__list_ennemies

     def is_ally(self, fighter: Fighter):
          for ally in self.get_list_allies():
               if fighter.get_antagonist().get_classe().get_name() is ally.get_antagonist().get_classe().get_name():
                    return True
          return False

     '''
          Function
     '''

     def start_turn(self) -> Fighters:
          '''
               We set a empty list to contains the fighters during the turn.
               We continue the turn while we have fighter in the priority queue.
               We take the next fighter in the priority queue.
               We take the action and the target that the fighter choose.
               If the action has one or more targets :
                    We take the number of target and let the player choose as many target that there is numbers of targets.
                    We apply the action to the target 
               If he don't have any targets, we juste apply the action.
               We append the fighter to the list of fighter.
               The we loop through the list of fighters to add each fighters  
          '''
          allies = []
          ennemies = []
          while not self.get_fighters().has_next_fighter() or (not self.get_list_allies() and not self.get_list_ennemies()):
               fighter = self.get_fighters().get_next_fighter()
               if fighter.get_antagonist().get_health().get_value() > 0:
                    action = fighter.choose_action()

                    if action.has_target():
                         if not action.has_multiple_choice():
                              target = fighter.choose_target(self.get_list_ennemies() if self.is_ally(fighter) else self.get_list_allies())
                         elif action.has_multiple_choice():
                              action = action.choose_action()
                              target = fighter.choose_target(self.get_list_ennemies() if self.is_ally(fighter) else self.get_list_allies(), action.get_nb_target())
                         else:
                              pass
                         if target:
                              if len(target) is 1:
                                   action.do_action(target)
                              else:
                                   for targ in list(target):
                                        action.do_action(target[targ])
                         else:
                              action.do_action()
                    else:
                         action.do_action(None)

               if fighter.get_antagonist().get_health().get_value() > 0:
                    if fighter in self.get_list_allies():
                         allies.append(fighter)
                    else:
                         ennemies.append(fighter)
          
          return allies, ennemies


     