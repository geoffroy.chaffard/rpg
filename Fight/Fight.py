from Fight.Tour.Round import Round
from Antagonist.Character.Fighters.Fighters import Fighters
from Antagonist.Character.Fighters.Ally import Ally
from Antagonist.Character.Fighters.Ennemy import Ennemy

class Fight():

     def __init__(self, allies, ennemies):
          self.__set_allies(allies)
          self.__set_ennemies(ennemies)
          self.__set_nb_allies(allies)
          self.__set_nb_ennemies(ennemies)

     def __set_allies(self, allies):
          self.__allies = {}
          if isinstance(allies, tuple) or isinstance(allies, dict) or isinstance(allies, list):
               for ally in allies:
                    self.__allies[ally.get_name()] = (ally, True)
          else:
               self.__allies[allies.get_name()] = (allies, True)

     def __set_nb_allies(self, allies):
          self.__nb_allies = len(allies)

     def get_allies(self):
          #Dictionnaire type fighter / vivant ou mort
          return self.__allies
     
     def get_nb_allies(self):
          return self.__nb_allies

     def __set_ennemies(self, ennemies):
          self.__ennemies = {}
          if isinstance(ennemies, tuple) or isinstance(ennemies, dict) or isinstance(ennemies, list):
               for ennemy in ennemies:
                    self.__ennemies[ennemy.get_name()] = (ennemy, True)
          else:
               self.__ennemies[ennemies.get_name()] = (ennemies, True)

     def __set_nb_ennemies(self, ennemies):
          self.__nb_ennemies = len(ennemies)

     def get_ennemies(self):
          return self.__ennemies

     def get_nb_ennemies(self):
          return self.__nb_ennemies

     def __set_counter(self):
          self.__counter = 0
     
     def get_counter(self):
          return self.__counter

     def add_turn_to_counter(self):
          self.__counter += 1
     
     def __set_turn(self, turn):
          self.__turn = turn

     def get_turn(self):
          return self.__turn

     def __start_new_turn(self):
          allies = []
          ennemies = []
          if self.get_nb_allies() > 0:
               for each_fighter in self.get_allies().keys():
                    (fighter, his_alive) = self.get_allies()[each_fighter]
                    if his_alive:
                         allies.append(fighter)
          else:
               raise Exception
          if self.get_nb_ennemies() > 0:
               for each_fighter in self.get_ennemies().keys():
                    (fighter, his_alive) = self.get_ennemies()[each_fighter]
                    if his_alive:
                         ennemies.append(fighter)
          else:
               raise Exception
          
          if allies and ennemies:
               turn = Round(allies, ennemies)
               allies, ennemies = turn.start_turn()
               self.__set_allies(allies)
               self.__set_ennemies(ennemies)
          else:
               raise Exception

     def has_end_fight(self):
          if self.is_all_dead(self.get_ennemies()):
               return (True, 'allies_victory')
          if self.is_all_dead(self.get_allies()):
               return (True, 'ennemy_victory')
          return (False, None)

     def is_all_dead(self, fighters):
          if isinstance(fighters, tuple) or isinstance(fighters, list):
               if not fighters[1]:
                    return True
               return False
          else:
               nb_fighters = len(fighters)
               cpt_dead = 0
               for fighter in fighters:
                    his_alive = fighters[fighter][1]
                    fighter = fighters[fighter][0] 
                    if not his_alive:
                         cpt_dead += 1
               if cpt_dead == nb_fighters:
                    return True
          return False

     def start(self):
          has_end_fight, end_fight = self.has_end_fight()
          while not has_end_fight:
               self.__start_new_turn()
               has_end_fight, end_fight = self.has_end_fight()
          return end_fight